# Stellar War

 Jhon é um civil que foi escolhido para lutar nas galaxias para salvar o mundo de um ataque de alienigenas, e para isso terá como
missão eliminar o numero certo de aliens para que consiga derrotá los e conseguir retornar para casa vivo! Para isso Jhon conta 
com a ajuda de estrelas que funcionam como suplementos para ajudar em sua sobrevivencia no espaço.

### Instalação

O usuário deve baixar todos os arquivos existentes no repositório raiz e fazer uso de algum tipo de IDE( eclipse, netbeans..)
sempre verificando se todas as pastas destinadas estão presentes e no local apropriado.

### Autor

* **Kamilla Costa Souza** - *16/0010969*


## Conhecimentos Abordados

* Orientação a Objeto(Uml, threads,agregação,composição...);
* Leitura/escrita em arquivo;
* Ordenação Simples;
* Uso de bibliotecas gráficas padrão do java.

