package jogo_ep2;
import java.awt.Rectangle;
import java.util.concurrent.ThreadLocalRandom;

public class Alien extends Sprite {

	private int type;
	private int horizontalSpeed = 2;
	private int verticalSpeed = 2;

	public Alien(int x, int y, int type) {
		super(x, y);
		

		this.type = type;
		this.x = ThreadLocalRandom.current().nextInt(0, 10000);
		loadAlien();

	} 

	public void loadAlien() {

		if (type == 1) {

			loadImage("res/alien_EASY.png");
		}
		if (type == 2) {

			loadImage("res/alien_MEDIUM.png");
		}
		if (type == 3) {

			loadImage("res/alien_HARD.png");
		}

	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void move() {

		y += verticalSpeed;
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, getWidth(), getHeight());
	}

	public int getHorizontalSpeed() {
		return horizontalSpeed;
	}

	public void setHorizontalSpeed(int horizontalSpeed) {
		this.horizontalSpeed = horizontalSpeed;
	}

	public void explosion() {
		loadImage("res/explosion.png");

	}
}
