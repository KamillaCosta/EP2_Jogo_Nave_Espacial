package jogo_ep2;
import java.awt.Rectangle;
import java.util.concurrent.ThreadLocalRandom;

public class Bonus extends Sprite {

	private int type;
	private int horizontalSpeed;
	private int verticalSpeed = 2;

	public Bonus(int x, int y, int type) {
		super(x, y);

		this.type = type;
		this.x = ThreadLocalRandom.current().nextInt(0, 100000);
		loadBonus();
	}

	public void loadBonus() {

		if (type == 1) {

			loadImage("res/star.png");
		}
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void move_Bonus() {

		y += verticalSpeed;
	}

	public void move() {

		y += verticalSpeed;
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, getWidth(), getHeight());
	}

	public int getHorizontalSpeed() {
		return horizontalSpeed;
	}

	public void setHorizontalSpeed(int horizontalSpeed) {
		this.horizontalSpeed = horizontalSpeed;
	}

	public void live() {
		loadImage("res/live.png");
	}

}
