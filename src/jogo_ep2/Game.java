package jogo_ep2;
public final class Game {

	private static final int WIDTH = 800;
	private static final int HEIGHT = 600;
	private static final int DELAY = 10;

	public static int getWidth() {
		return WIDTH;
	}

	public static int getHeight() {
		return HEIGHT;
	}

	public static int getDelay() {
		return DELAY;
	}

}
