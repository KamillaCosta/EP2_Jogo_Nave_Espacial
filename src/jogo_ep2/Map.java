package jogo_ep2;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	private List<Alien> aliens = new ArrayList<Alien>();
	private List<Bonus> bonus = new ArrayList<Bonus>();

	private final Timer mapTimer;
        
        private Sound som; //SOM BEM AQUI DECLARANDO TIPO SOM

        private Sound somfundo;
        private int chave = 1;
	private final Image background;
	private final SpaceShip spaceship;

	private boolean rodandoGame;

	public Map() {
		final int SPACESHIP_X = 400;
		final int SPACESHIP_Y = 500;

		addKeyListener(new KeyListerner());

		setFocusable(true);
		setDoubleBuffered(true);

		ImageIcon image = new ImageIcon("res/space.jpg");
                som = new Sound("boom"); //INSTANCIANDO SOM
                somfundo = new Sound("Guardians Of The Galaxy - Come And Get Your Love - Redbone");
		this.background = image.getImage();

		spaceship = new SpaceShip(SPACESHIP_X, SPACESHIP_Y);

		rodandoGame = true;

		mapTimer = new Timer(Game.getDelay(), this);
		mapTimer.start();
	}

	private void initAlien() {

		if (spaceship.getScore() < 100) {
			aliens.add(new Alien(0, 0, 1));
		}
		if (spaceship.getScore() >= 100 && spaceship.getScore() < 200) {
			aliens.add(new Alien(0, 0, 2));
		}
		if (spaceship.getScore() >= 200) {
			aliens.add(new Alien(0, 0, 3));
		}

	}

	private void initBonus() {
		int i = 0;
		if (i % 10 == 0)
			bonus.add(new Bonus(0, 0, 1));
		i++;
	}

	private void drawMissile(Graphics g) {
		List<Missile> missiles = spaceship.getMissiles();
		missiles = spaceship.getMissiles();

		for (int i = 0; i < missiles.size(); i++) {
			Missile m = missiles.get(i);
			g.drawImage(m.getImage(), m.getX(), m.getY(), this);
		}

		g.dispose();

	}

	private void drawAlien(Graphics g) {
		for (int i = 0; i < aliens.size(); i++) {
			Alien a = aliens.get(i);
			g.drawImage(a.getImage(), a.getX(), a.getY(), this);

		}
	}

	private void drawBonus(Graphics g) {
		for (int i = 0; i < bonus.size(); i++) {
			Bonus b = bonus.get(i);
			g.drawImage(b.getImage(), b.getX(), b.getY(), this);
		}

	}

	private void updateMissile() {

		List<Missile> missiles = spaceship.getMissiles();

		for (int i = 0; i < missiles.size(); i++) {

			Missile m = missiles.get(i);

			if (m.isVisible()) {
				m.move();
			} else {
				missiles.remove(i);
			}
		}
	}

	private void updateAlien() {
		if (aliens.isEmpty()) {
			initAlien();
		}

		for (int i = 0; i < aliens.size(); i++) {
			Alien a = aliens.get(i);
			if (a.isVisible()) {
				a.move();

			} else {
				aliens.remove(i);
			}
		}
	}

	private void updateBonus() {

		if (bonus.isEmpty()) {
			initBonus();
		}

		for (int i = 0; i < bonus.size(); i++) {
			Bonus b = bonus.get(i);
			if (b.isVisible()) {
				b.move_Bonus();

			} else {
				bonus.remove(i);
			}
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
                if(chave == 1 ){
                somfundo.getSom().loop();
                chave = 0;
                }
		spaceship.updateSpaceship();
		updateMissile();
		initAlien();
		updateAlien();
		initBonus();
		updateBonus();
		checaColisoes();
                
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(this.background, 0, 0, null);

		if (rodandoGame) {

			List<Missile> missiles = spaceship.getMissiles();

			for (int i = 0; i < missiles.size(); i++) {
				Missile m = (Missile) missiles.get(i);
				g.drawImage(m.getImage(), m.getX(), m.getY(), this);

			}

			for (int i = 0; i < aliens.size(); i++) {
				Alien a = aliens.get(i);
				g.drawImage(a.getImage(), a.getX(), a.getY(), this);

			}

			for (int i = 0; i < bonus.size(); i++) {
				Bonus b = bonus.get(i);
				g.drawImage(b.getImage(), b.getX(), b.getY(), this);
			}

		} else {

			GameOver(g);
		}
		score(g, String.valueOf(spaceship.getScore()));
		lives(g, String.valueOf(spaceship.getLive()));
		if (spaceship.getScore() >= 300) {
			Victory(g);
		}
		spaceship.drawSpaceship(g, this);
		drawAlien(g);
		drawMissile(g);
		drawBonus(g);

		

		Toolkit.getDefaultToolkit().sync();
	}

	private void Victory(Graphics g) {

		String message = "Victory";
		Font font = new Font("Arial", Font.BOLD, 50);
		FontMetrics metric = getFontMetrics(font);
		g.setColor(Color.green);
		g.setFont(font);
		g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
		mapTimer.stop();
	}

	private void GameOver(Graphics g) {

		String message = "Game Over";
		Font font = new Font("Arial", Font.BOLD, 50);
		FontMetrics metric = getFontMetrics(font);
		g.setColor(Color.red);
		g.setFont(font);
		g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
		mapTimer.stop();
	}

	private void score(Graphics g, String score) {
		String message = "Score:" + score;
		Font font = new Font("Arial", Font.BOLD, 18);
		FontMetrics metric = getFontMetrics(font);
		g.setColor(Color.yellow);
		g.setFont(font);
		g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 20, Game.getHeight() / 20);
	}

	private void lives(Graphics g, String lives) {
		String message = "Lives:" + lives;
		Font font = new Font("Arial", Font.BOLD, 18);
		FontMetrics metric = getFontMetrics(font);
		g.setColor(Color.yellow);
		g.setFont(font);
		g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 20, Game.getHeight() / 10);

	}

	private class KeyListerner extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {
			spaceship.keyPressed(e);
		}

		@Override
		public void keyReleased(KeyEvent e) {
			spaceship.keyReleased(e);
		}

	}

	public void checaColisoes() {

		Rectangle Nave = spaceship.getBounds();
		Rectangle Alienigena;
		Rectangle Missil;
		Rectangle Bonuss;

		for (int i = 0; i < aliens.size(); i++) {
			Alien tempAlien = aliens.get(i);
			Alienigena = tempAlien.getBounds();

			if (Nave.intersects(Alienigena)) {
				tempAlien.explosion();
				spaceship.setVisible(false);
				tempAlien.setVisible(false);
				spaceship.downScore();
				spaceship.lostLives();
                                som.getSom().play();
                                
				if (!spaceship.hasLive()) {
					spaceship.explosion();
					spaceship.setVisible(false);

					rodandoGame = false;
				}

			}
		}

		List<Missile> missiles = spaceship.getMissiles();

		for (int i = 0; i < missiles.size(); i++) {
			Missile tempMissile = missiles.get(i);
			Missil = tempMissile.getBounds();

			for (int j = 0; j < aliens.size(); j++) {
				Alien tempAlien = aliens.get(j);
				Alienigena = tempAlien.getBounds();
                                    
				if (Missil.getMaxY() < Alienigena.getMinY()
						&& (Missil.getMinX() < Alienigena.getMaxX() && Alienigena.getMinX() < Missil.getMaxX())) {
					tempAlien.explosion();
                                        som.getSom().play();
					tempAlien.setVisible(false);
					tempMissile.setVisible(false);
					spaceship.upScore();

				}
			}
		}

		for (int i = 0; i < bonus.size(); i++) {
			Bonus tempBonus = bonus.get(i);
			Bonuss = tempBonus.getBounds();

			if (Nave.intersects(Bonuss)) {
				tempBonus.live();
				spaceship.setVisible(true);
				tempBonus.setVisible(false);
				spaceship.gainLives();

			}
		}
	}
}

