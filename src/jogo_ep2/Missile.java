package jogo_ep2;
import java.awt.Rectangle;
import java.util.ArrayList;
import javax.swing.ImageIcon;

public class Missile extends Sprite {

	private static final int MISSILE_WIDTH = 800;
	private static final int MISSILE_SPEED = 40;
	private ArrayList<Missile> missiles;

	public Missile(int x, int y) {
		super(x, y);

		ImageIcon ii = new ImageIcon("res/missile.png");
		image = ii.getImage();

	}

	public void move() {
		this.y -= MISSILE_SPEED;

		if (this.y > MISSILE_WIDTH) {
			setVisible(false);
		}
	}

	public void shoot() {
		missiles.add(new Missile(x + 6, y));
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, getWidth(), getHeight());
	}

	public void explosion() {
		loadImage("res/explosion.png");
	}
}
