package jogo_ep2;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class SpaceShip extends Sprite {

	private static final int MAX_SPEED_X = 2;
	private static final int MAX_SPEED_Y = 2;

	private int horizontalSpeed;
	private int verticalSpeed;
	private int score;
	private int lives = 5;

	private ArrayList<Missile> missiles;

	public SpaceShip(int x, int y) {
		super(x, y);

		initSpaceShip();
	}

	private void initSpaceShip() {
		this.missiles = new ArrayList<Missile>();
		noThrust();
	}
	
	public void drawSpaceship(Graphics g, Map map) {

		g.drawImage(this.getImage(), this.getX(), this.getY(), map);
	}
	
	public void updateSpaceship() {
		this.move();
	}

	public ArrayList<Missile> getMissiles() {
		return missiles;
	}

	public void shoot() {
		missiles.add(new Missile(x + 6, y));
	}

	private void noThrust() {
		loadImage("res/spaceship.png");
	}

	private void thrust() {
		loadImage("res/spaceship_thrust.png");
	}

	public void move() {

		if (hasReachedHorizontalBoundaries()) {
			horizontalSpeed = 0;
		}

		x += horizontalSpeed;

		if (hasReachedVerticalBoundaries()) {
			verticalSpeed = 0;
		}

		y += verticalSpeed;
	}

	public boolean hasReachedHorizontalBoundaries() {
		return (horizontalSpeed < 0 && x <= 0) || (horizontalSpeed > 0 && x + width >= Game.getWidth());
	}

	public boolean hasReachedVerticalBoundaries() {
		return (verticalSpeed < 0 && y <= 0) || (verticalSpeed > 0 && y + 1.8 * height >= Game.getHeight());
	}

	public void keyPressed(KeyEvent e) {

		int key = e.getKeyCode();
		switch (key) {

		case KeyEvent.VK_SPACE:
			shoot();

			break;

		case KeyEvent.VK_LEFT:
			horizontalSpeed = -1 * MAX_SPEED_X;
			break;

		case KeyEvent.VK_RIGHT:
			horizontalSpeed = MAX_SPEED_X;
			break;

		case KeyEvent.VK_UP:
			verticalSpeed = -1 * MAX_SPEED_Y;
			thrust();
			break;

		case KeyEvent.VK_DOWN:
			verticalSpeed = MAX_SPEED_Y;
			break;
		}
	}

	public void keyReleased(KeyEvent e) {

		int key = e.getKeyCode();

		switch (key) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_RIGHT:
			horizontalSpeed = 0;
			break;

		case KeyEvent.VK_UP:
		case KeyEvent.VK_DOWN:
			verticalSpeed = 0;
			noThrust();
			break;
		}
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, getWidth(), getHeight());
	}

	public void explosion() {
		loadImage("res/explosion.png");
	}

	public void live() {
		loadImage("res/live.png");
	}

	public boolean hasLive() {
		if (lives != 0) {
			return true;
		} else
			return false;
	}

	public void lostLives() {
		lives--;
	}

	public void gainLives() {
		lives++;
	}

	public void upScore() {
		score++;
	}

	public void downScore() {
		score--;
	}
	
	public int getScore(){
		return score;
	}
	
	public int getLive(){
		return lives;
	}
}