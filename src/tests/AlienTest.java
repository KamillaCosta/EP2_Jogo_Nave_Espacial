package tests;
import static org.junit.Assert.*;
import org.junit.Test;
import jogo_ep2.Alien;;

public class AlienTest {

	@Test
	public void testCreateAlien() {

		Alien alien = new Alien(0,2,1);
		assertTrue(alien.getType() == 1);
		assertTrue(alien.getY() == 2);
	}
	
	public void testMove(){
		
		Alien alien = new Alien(0,5,2);
		alien.move();
		assertTrue(alien.getY() == 7);
	}
}
