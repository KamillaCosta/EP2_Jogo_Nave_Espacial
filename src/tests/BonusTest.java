package tests;
import static org.junit.Assert.*;
import org.junit.Test;
import jogo_ep2.Bonus;

public class BonusTest {

	@Test
	public void testCreateBonus() {

		Bonus bonus = new Bonus(0,3,1);
		assertTrue(bonus.getType() == 1);
		assertTrue(bonus.getY() == 3);
	}

}
