package tests;
import static org.junit.Assert.*;
import org.junit.Test;
import jogo_ep2.Missile;


public class MissileTest {
	@Test
	public void testCreateMissile() {
		
		Missile missile = new Missile (3,5);
		assertTrue(missile.getX() == 3);
		assertTrue(missile.getY() == 5);
	}

}
