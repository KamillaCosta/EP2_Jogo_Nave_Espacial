package tests;
import static org.junit.Assert.*;
import org.junit.Test;
import jogo_ep2.SpaceShip;

public class SpaceShipTest {

	@Test
	public void testCreateSpaceChip() {
		
		SpaceShip spaceship = new SpaceShip (2,1);
		assertTrue(spaceship.getX() == 2);
		assertTrue(spaceship.getY() == 1);
		
	}

}
